require("dotenv").config();
const fs = require("fs");

const CHAR = Number(process.env.CHAR);
const isInt = isNaN(CHAR) ? false : true;

fs.writeFileSync(
  "./output.json",
  JSON.stringify({
    CHAR,
    isInt,
  })
);
